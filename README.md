Subject: installing gitlab with docker

This is the list of commends that i ran:

apt install docker.io containerd docker-compose

systemctl start docker
systemctl enable docker

systemctl status docker
docker run hello-world

mkdir -p gitlab; cd gitlab/

mkdir -p /srv/gitlab/{config/ssl,logs,data}

edited vim file .env

GITLAB_HOME=/srv/gitlab

edited vim docker-compose.yml file

apt install certbot

certbot certonly --rsa-key-size 2048 --standalone --agree-tos --no-eff-email --email user@hakase-labs.io -d gitlab.hakase-labs.io

copy the certificate file "fullchain.pem" and "privkey.pem" to the "/srv/gitlab/config/ssl/" directory
cp /etc/letsencrypt/live/gitlab.hakase-labs.io/fullchain.pem /srv/gitlab/config/ssl/
cp /etc/letsencrypt/live/gitlab.hakase-labs.io/privkey.pem /srv/gitlab/config/ssl/

openssl dhparam -out /srv/gitlab/config/ssl/dhparams.pem 2048

Build the GitLab Container

ls -lah ~/gitlab/
tree /srv/gitlab

cd ~/gitlab
docker-compose up -d
docker-compose ps
ss -plnt

Check GitLab Service Status inside the Container

docker exec -it gitlab_web_1 gitlab-ctl status

Access GitLab Container

docker exec -it gitlab_web_1 /bin/bash

cat /etc/lsb-release

Edit the GitLab Configuration gitlab.rb

docker exec -it gitlab_web_1 editor /etc/gitlab/gitlab.rb

docker restart gitlab_web_1

docker-compose ps
